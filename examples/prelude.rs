use std::rc::Rc;

use gdk::gio::ApplicationFlags;
use gdk::prelude::{ApplicationExt, ApplicationExtManual};
use gtk::gdk;
use gtk::prelude::WidgetExt;
use libadwaita as adw;

use grx::prelude::*;

fn main() {
    let app = adw::Application::new(Some("codes.loers.Test"), ApplicationFlags::empty());

    app.connect_startup(|app| {
        let c = grx! {
            clamp (max_width=400) [
                container(styles=[vertical, spacing(10)]) [
                    container(styles=[p(1), hover:m(2), spacing(10)]) [
                        "Label",
                        button(
                            styles=[p(10)],
                            on_click=|| {
                                println!("test1");
                                println!("test2");
                            }
                        ) [
                            "Test"
                        ]
                    ],
                    scroll (styles=[vexpand]) [
                        container(styles=[vertical, m(10), spacing(10)]) [
                            list (classes="boxed-list".into()) [
                                action_row (
                                    title="Test".into(),
                                    subtitle="Foo Bar".into()
                                ) [
                                    button (styles=[valign(center)]) [
                                        "Test"
                                    ]
                                ],
                                action_row (
                                    title="Test".into(),
                                    subtitle="Foo Bar".into()
                                ) [
                                    button (styles=[valign(center)]) [
                                        "Test"
                                    ]
                                ],
                                expander_row (
                                    title="Test".into(),
                                    subtitle="Foo Bar".into(),
                                    selectable=Some(false)
                                ) [
                                    container (styles=[spacing(100), vertical]) [
                                        button (styles=[valign(center)]) [
                                            "Test"
                                        ],
                                        button (styles=[valign(center)]) [
                                            "Test"
                                        ]
                                    ]
                                ]
                            ],
                            list (classes="boxed-list".into()) [
                                action_row (
                                    title="Test".into(),
                                    subtitle="Foo Bar".into()
                                ) [
                                    button (styles=[valign(center)]) [
                                        "Test"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        };

        let inner = c.inner();
        let w: &gtk::Widget = inner.downcast_ref().unwrap();

        let window = gtk::ApplicationWindow::builder()
            .application(app)
            .child(w)
            .width_request(200)
            .height_request(600)
            .build();
        window.show();
    });
    app.run();
}
