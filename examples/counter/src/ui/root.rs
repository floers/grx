use grx::{component, props, container, grx, Component};

use crate::ui::{header, main};

#[derive(Default)]
pub(crate) struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct State {}

#[component(Props, State)]
pub struct Root {}

pub fn root() -> Component {
    grx! {
        container(styles=[h(100), vertical]) [
            header,
            main
        ]
    }
}
