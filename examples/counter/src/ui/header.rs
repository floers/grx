use std::rc::Rc;

use grx::{
    grx,
    menu::{self, Menu},
    prelude::*,
    props, Component,
};

use crate::store::Action;

#[props]
#[derive(Debug, Default)]
pub struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct State {}

#[component(Props, State)]
pub struct Root {}

pub fn header(_props: Props) -> Component {
    // let menu: Rc<Menu>;
    grx! {
        header_bar 
        // [
            // menu (id=menu, items=vec![
            //     menu_item("Open".into(), Some(Action::Start)),
            //     menu_item("Quit".into(), Some(Action::Quit)),
            // ]),
            // button (on_click=move || menu.show()) [
            //     icon("open-menu-symbolic")
            // ]
        // ]
    }
}
