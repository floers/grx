use std::rc::Rc;

use grx::{component, container, grx, props, text, Component, Text, Textual};

use crate::store::{store, SELECT_NOTIFICATION};

#[props]
#[derive(Debug, Default)]
pub struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct State {}

#[component(Props, State)]
pub struct Main {}

pub fn main(props: Props) -> Rc<Main> {
    let txt: Rc<Text>;

    let component = grx! {
        container (styles = [p(4), vertical]) [
            "Go to menu > start",
            "Notification will be here: ",
            text(id=txt)
        ]
    };

    let main = Main::new(props, component);

    main.later_drop(store().select(SELECT_NOTIFICATION, move |s| {
        if let Some(msg) = s.notification.message.as_ref() {
            txt.set_text(msg);
        } else {
            txt.set_text("");
        }
    }));

    main
}
