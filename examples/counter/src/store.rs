use std::{fmt::Debug, rc::Rc};

use gstore::Actionable;

use self::notification::{action::NotificationAction, NotificationMiddleware};

mod notification;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Action {
    Quit,
    Start,
    Notification(NotificationAction),
}

impl Actionable for Action {
    fn name(&self) -> &'static str {
        match self {
            Action::Quit => "app.quit",
            Action::Start => "app.start",
            Action::Notification(NotificationAction::Hide) => "app.hide",
            Action::Notification(NotificationAction::Show(_)) => "app.show",
        }
    }

    fn list() -> Vec<Self> {
        vec![Action::Quit, Action::Start]
    }

    fn try_from_name(name: &str) -> Option<Self>
    where
        Self: Sized,
    {
        match name {
            "quit" => Some(Self::Quit),
            "start" => Some(Self::Start),
            _ => None,
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct State {
    pub notification: notification::Slice,
}

pub fn append_middlewares(store: &Rc<Store>) {
    store.append(Box::new(NotificationMiddleware))
}

pub fn reduce(action: &Action, state: &mut State) {
    notification::reduce(action, state);
}

gstore::store!(Action, State);

// pub selectors

pub const SELECT_NOTIFICATION: &'static Selector = &|a, b| a.notification != b.notification;
