use gtk::{
    prelude::{ApplicationExt, ApplicationExtManual},
    traits::WidgetExt,
};
use libadwaita as adw;

mod store;
mod ui;

pub use grx;

use crate::store::Action;

#[async_std::main]
async fn main() {
    env_logger::init();

    let app = adw::Application::builder()
        .application_id("codes.loers.Counter")
        .build();

    let store = store::store();

    // register middlewares
    store::append_middlewares(&store);

    // bind store to gtk
    let moved_app = app.clone();
    store.bind_gtk(&app, move |a, s| {
        // call root reducer
        store::reduce(a, s);

        // quit app on quit action
        if a == &Action::Quit {
            moved_app.quit();
        }
    });

    // run app
    app.connect_startup(|app| {
        let inner = ui::root::root().inner();
        let w: &gtk::Widget = inner.downcast_ref().unwrap();

        let window = adw::ApplicationWindow::builder()
            .application(app)
            .title("Counter")
            .content(w)
            .build();
        window.show();
    });

    app.run();
}
