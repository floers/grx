use std::sync::Arc;

use async_trait::async_trait;
use gstore::Middleware;

use self::action::NotificationAction;

use super::{store, Action, State};

pub mod action {
    use crate::store::Action;

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub enum NotificationAction {
        Show(String),
        Hide,
    }
    impl From<NotificationAction> for Action {
        fn from(value: NotificationAction) -> Self {
            Action::Notification(value)
        }
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct Slice {
    pub message: Option<String>,
}

pub fn reduce(action: &Action, state: &mut State) {
    match action {
        Action::Notification(NotificationAction::Show(message)) => {
            state.notification.message = Some(message.clone());
        }
        Action::Notification(NotificationAction::Hide) => {
            state.notification.message = None;
        }
        Action::Start => state.notification.message = Some(" Wait for it!".into()),
        _ => {
            // ignore
        }
    }
}

pub struct NotificationMiddleware;
#[async_trait]
impl Middleware<Action, State> for NotificationMiddleware {
    async fn pre_reduce(&self, a: Arc<Action>, _: Arc<State>) {
        if a.as_ref() == &Action::Start {
            async_std::task::sleep(std::time::Duration::from_millis(2000)).await;
            store().dispatch(NotificationAction::Show("Pre reduce notification!".into()))
        }
    }

    async fn post_reduce(&self, a: Arc<Action>, _: Arc<State>) {
        if a.as_ref() == &Action::Start {
            async_std::task::sleep(std::time::Duration::from_millis(5000)).await;
            store().dispatch(NotificationAction::Show("Post reduce notification!".into()))
        }
    }
}
