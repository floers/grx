// SPDX-License-Identifier: GPL-3.0-or-later

//! Types for callback handlers used in grx components.

use std::rc::Rc;

pub type Handler<L> = Rc<dyn Fn(std::rc::Rc<L>)>;
