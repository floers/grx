// SPDX-License-Identifier: GPL-3.0-or-later

//! Annotations for grx components.
//!
//! grx components can annotate their children in some cases to e.g. have a certain alignment or position.

pub type Annotations = std::collections::HashMap<String, serde_json::Value>;
