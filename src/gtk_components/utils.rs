use gtk::prelude::{Cast, IsA, WidgetExt};

pub fn get_nth_child<T: IsA<gtk::Widget>>(widget: &impl IsA<gtk::Widget>, n: u32) -> Option<T> {
    let mut c = widget.first_child();
    let mut i = 0;
    while let Some(child) = c {
        c = child.next_sibling();
        if i == n {
            child.set_hexpand(true);
            let w = child.downcast::<T>();
            match w {
                Ok(w) => return Some(w),
                Err(e) => {
                    log::warn!("Could not cast child {n} to T: {e}");
                }
            }
        }
        i += 1;
    }
    log::warn!("Could not find {} in {:?}.", n, widget);
    None
}
