// SPDX-License-Identifier: GPL-3.0-or-later

//! The basic container component (GTK Box).

use super::gtk_props::apply;
use crate::{new_gc, props};
use glib::prelude::Cast;
use grx_macros::gtk_component;
use gtk::glib;
use gtk::prelude::WidgetExt;
use std::rc::Rc;

#[props]
#[derive(Default, Debug)]
pub struct Props {}

pub fn window(mut props: Props) -> Rc<Window> {
    let mut win = libadwaita::Window::builder();
    win = win.hide_on_close(true);
    if let Some(child) = props.children.first() {
        let inner = child.inner();
        let w: &gtk::Widget = inner.downcast_ref().unwrap();
        win = win.content(w);
    }
    let win = win.build();
    let comp = new_gc!(Window { win, props });
    apply(comp.clone());
    comp
}

#[gtk_component(libadwaita::Window)]
#[derive(Debug)]
pub struct Window {}

impl Window {
    pub fn show(self: &Rc<Self>) {
        self.widget.show();
    }
}
