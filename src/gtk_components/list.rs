// SPDX-License-Identifier: GPL-3.0-or-later

//! The basic container component (GTK Box).

use glib::prelude::Cast;
use grx_macros::gtk_component;
use gtk::glib;
use std::rc::Rc;

use crate::{container_ext, new_gc, props, ComponentExt, ContainerExt};

use super::gtk_props::apply;

#[props]
#[derive(Default, Debug)]
pub struct Props {}

pub fn list(mut props: Props) -> Rc<List> {
    let list = gtk::ListBox::builder().build();
    let comp = new_gc!(List { list, props });
    for c in comp.children().iter() {
        comp.append(c.clone());
    }
    apply(comp.clone());
    comp
}

#[gtk_component(gtk::ListBox)]
#[derive(Debug)]
pub struct List {}

container_ext! {List}
