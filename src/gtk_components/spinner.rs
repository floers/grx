// SPDX-License-Identifier: GPL-3.0-or-later

//! An animated loading spinner

use super::gtk_props::apply;
use crate::new_gc;
use glib::prelude::Cast;
use grx_macros::{gtk_component, props};
use gtk::glib;
use std::rc::Rc;

#[props]
#[derive(Default, Debug)]
pub struct Props {}

pub fn spinner(mut props: Props) -> Rc<Spinner> {
    let gtk_spinner = gtk::Spinner::builder().spinning(true).build();
    let w = gtk_spinner;
    let comp = new_gc!(Spinner { w, props });
    apply(comp.clone());
    comp
}

#[gtk_component(gtk::Spinner)]
#[derive(Debug)]
pub struct Spinner {}
