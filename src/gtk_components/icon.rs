// SPDX-License-Identifier: GPL-3.0-or-later

//! GTKs image component
use std::rc::Rc;

use glib::prelude::Cast;
use grx_macros::{gtk_component, props};
use gtk::glib;

use crate::new_gc;

#[props]
#[derive(Debug, Default)]
pub struct Props {
    pub name: &'static str,
}

pub fn icon(mut props: Props) -> Rc<Icon> {
    let img = gtk::Image::builder().icon_name(props.name).build();
    new_gc!(Icon { img, props })
}

#[gtk_component(gtk::Image)]
#[derive(Debug)]
pub struct Icon {}
