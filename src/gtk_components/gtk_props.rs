use std::rc::Rc;

use gtk::prelude::WidgetExt;

use crate::Component;

pub fn apply(component: Component) {
    let props = component.props();
    let widget: Rc<gtk::Widget> = component.inner().downcast().unwrap();
    if let Some(id) = props.id() {
        widget.set_widget_name(id);
    }
    if let Some(classes) = props.classes() {
        for cls in classes.split(' ') {
            widget.add_css_class(cls);
        }
    }
}
