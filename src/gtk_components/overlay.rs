// SPDX-License-Identifier: GPL-3.0-or-later

//! The basic container component (GTK Box).

use std::rc::Rc;

use glib::prelude::Cast;
use grx_macros::gtk_component;
use gtk::glib;

use crate::{default_interactable, new_gc, props};

use super::gtk_props::apply;

#[props]
#[derive(Default, Debug)]
pub struct Props {}

pub fn overlay(mut props: Props) -> Rc<Overlay> {
    let overlay = gtk::Overlay::builder().build();
    for c in &props.children {
        let inner = c.clone().inner();
        let w: &gtk::Widget = inner.downcast_ref().unwrap();
        if c.annotations().get("overlay").is_some() {
            overlay.add_overlay(w)
        } else {
            overlay.set_child(Some(w))
        }
    }
    let comp = new_gc!(Overlay { overlay, props });
    apply(comp.clone());
    comp
}

#[gtk_component(gtk::Overlay)]
#[derive(Debug)]
pub struct Overlay {}

default_interactable! {Overlay}
