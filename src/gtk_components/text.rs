// SPDX-License-Identifier: GPL-3.0-or-later

use std::rc::Rc;

use crate::{gtk_props::apply, new_gc, Textual};
use glib::prelude::Cast;
use grx_macros::{gtk_component, props};
use gtk::glib;

#[props]
#[derive(Debug, Default)]
pub struct Props {
    pub text: String,
}

pub fn text(mut props: Props) -> Rc<Text> {
    let widget = gtk::Label::builder().label(&props.text).wrap(true).build();
    let widget = widget.upcast();
    let comp = new_gc!(Text { widget, props });
    apply(comp.clone());
    comp
}

#[gtk_component(gtk::Label)]
#[derive(Debug)]
pub struct Text {}

impl Text {
    /// Changes visibility
    pub fn set_visible(self: &Rc<Self>, visible: bool) {
        gtk::prelude::WidgetExt::set_visible(&self.widget, visible);
    }
}

impl Text {
    pub fn new(s: &str) -> Rc<Text> {
        text(Props {
            text: s.into(),
            ..Default::default()
        })
    }
}

impl Textual for Text {
    fn set_text(self: &Rc<Self>, txt: &str) {
        self.widget.set_text(txt);
    }

    fn text(self: &Rc<Self>) -> String {
        self.widget.text().to_string()
    }
}
