use glib::prelude::ObjectExt;
use gtk::glib;

use crate::ItemExt;

glib::wrapper! {
    pub struct Item(ObjectSubclass<imp::Item>);
}

impl Item {
    pub fn new(id: &str, value: &str) -> Self {
        glib::Object::builder()
            .property("id", id)
            .property("value", value)
            .build()
    }
}

mod imp {
    use glib::{prelude::ObjectExt, ParamSpec, Properties, Value};
    use gtk::glib;
    use gtk::subclass::prelude::*;
    use std::cell::RefCell;

    #[derive(Properties, Default)]
    #[properties(wrapper_type = super::Item)]
    pub struct Item {
        #[property(get, set)]
        id: RefCell<String>,
        #[property(get, set)]
        value: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Item {
        const NAME: &'static str = "Item";
        type Type = super::Item;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for Item {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }

        fn constructed(&self) {
            self.parent_constructed();
        }
    }
}

impl ItemExt for Item {
    fn id(&self) -> String {
        self.property("id")
    }

    fn value(&self) -> String {
        self.property("value")
    }
}
