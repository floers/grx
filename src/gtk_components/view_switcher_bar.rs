// SPDX-License-Identifier: GPL-3.0-or-later

//! A bar that allows to switch between views.

use std::rc::Rc;

use glib::prelude::Cast;
use grx_macros::gtk_component;
use gtk::glib;

use crate::{new_gc, props, view_stack::ViewStack, ComponentExt};

use super::gtk_props::apply;

#[props]
#[derive(Default, Debug)]
pub struct Props {
    pub stack: Option<Rc<ViewStack>>,
}

pub fn view_switcher_bar(mut props: Props) -> Rc<ViewSwitcherBar> {
    let widget = libadwaita::ViewSwitcherBar::builder().build();
    widget.set_reveal(true);
    if let Some(stack) = &props.stack {
        let stack: Rc<gtk::Widget> = stack.clone().inner().downcast().unwrap();
        widget.set_stack(stack.downcast_ref());
    }

    let widget: gtk::Widget = widget.upcast();
    let comp = new_gc!(ViewSwitcherBar { widget, props });
    apply(comp.clone());
    comp
}

#[gtk_component(gtk::Widget)]
#[derive(Debug)]
pub struct ViewSwitcherBar {}
