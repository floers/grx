// SPDX-License-Identifier: GPL-3.0-or-later

//! The basic container component (GTK Box).

use std::rc::Rc;

use glib::prelude::Cast;
use grx_macros::gtk_component;
use gtk::glib;
use gtk::prelude::BoxExt;

use crate::{container_ext, default_interactable, new_gc, props};

use super::gtk_props::apply;

#[props]
#[derive(Default, Debug)]
pub struct Props {}

pub fn container(mut props: Props) -> Rc<Container> {
    let gbox = gtk::Box::builder().build();
    for c in &props.children {
        let inner = c.clone().inner();
        let w: &gtk::Widget = inner.downcast_ref().unwrap();
        gbox.append(w);
    }
    let comp = new_gc!(Container { gbox, props });
    apply(comp.clone());
    comp
}

#[gtk_component(gtk::Box)]
#[derive(Debug)]
pub struct Container {}

container_ext! {Container}

default_interactable! {Container}
