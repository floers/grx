// SPDX-License-Identifier: GPL-3.0-or-later

//! Props types for grx components.

use std::rc::Rc;

use crate::styles::Style;

pub trait ExtendingProps: std::fmt::Debug {
    fn id(&self) -> Option<&str>;
    fn styles(&self) -> Option<&Vec<Style>>;
    fn classes(&self) -> Option<&str>;
}

#[derive(Clone)]
pub struct PropsPointer {
    props: Rc<dyn ExtendingProps>,
}
impl std::fmt::Debug for PropsPointer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("PropsPointer")
            .field("id", &self.props.id())
            .field("classes", &self.props.classes())
            .field("styles", &self.props.styles())
            .finish()
    }
}

impl From<Rc<dyn ExtendingProps>> for PropsPointer {
    fn from(value: Rc<dyn ExtendingProps>) -> Self {
        Self { props: value }
    }
}

#[cfg(feature = "gtk")]
pub use crate::gtk_components::gtk_props::apply;
